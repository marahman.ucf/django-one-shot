from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todos = get_object_or_404(TodoList, id=id)
    context = {
        "todos_object": todos,
    }
    return render(request, "todos/details.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form_todo = form.save()
            return redirect("todo_list_detail", id=form_todo.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todos = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todos)
        if form.is_valid():
            form_todo = form.save()
            return redirect("todo_list_detail", id=form_todo.id)
    else:
        form = TodoListForm(instance=todos)

    context = {
        "form": form,
        "todos": todos,
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todo_list_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }
    return render(request, "todos/item-create.html", context)


def todo_item_update(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=todo_item)

    context = {
        "form": form,
        "todo_item": todo_item,
    }
    return render(request, "todos/item-update.html", context)
